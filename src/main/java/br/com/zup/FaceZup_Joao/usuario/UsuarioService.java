package br.com.zup.FaceZup_Joao.usuario;

import br.com.zup.FaceZup_Joao.exceptions.UsuarioDuplicadoException;
import br.com.zup.FaceZup_Joao.exceptions.UsuarioNaoEncontradoException;
import br.com.zup.FaceZup_Joao.mensagem.Mensagem;
import br.com.zup.FaceZup_Joao.mensagem.MensagemService;
import br.com.zup.FaceZup_Joao.post.Post;
import br.com.zup.FaceZup_Joao.post.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private MensagemService mensagemService;
    @Autowired
    private PostService postService;

    private void cadastrarUsuarioSistema(){
        Usuario usuarioSistema = new Usuario();
        usuarioSistema.setEmail("sistema@email.com");
        usuarioSistema.setNome("SISTEMA");
        usuarioSistema.setSobrenome("SISTEMA");
        usuarioSistema.setCargo("AUTOMATIZAR");

        this.usuarioRepository.save(usuarioSistema);
    }

    public void cadastrarUsuario(Usuario usuario){
        if (!this.usuarioRepository.existsById("sistema@email.com")){
            cadastrarUsuarioSistema();
        }
        verificaUsuarioDuplicado(usuario.getEmail());
        this.usuarioRepository.save(usuario);
    }

    public void verificaUsuarioDuplicado(String emailUsuario){
        if (this.usuarioRepository.existsById(emailUsuario)){
            throw new UsuarioDuplicadoException("Usuario já cadastrado!!!");
        }
    }

    public Usuario pesquisarUsuarioPorEmail(String email){
        pesquisarUsuarioExiste(email);
        return this.usuarioRepository.findById(email).get();
    }

    public boolean pesquisarUsuarioExiste(String emailUsuario){
        if (this.usuarioRepository.existsById(emailUsuario)){
            return true;
        }
        throw new UsuarioNaoEncontradoException("Usuário não encontrado!!!");
    }

    public List<Mensagem> pesquisaListaIdMensagemNaoLida(String emailUsuario){
        pesquisarUsuarioExiste(emailUsuario);
        return this.mensagemService.pesquisaListaDeIdMensagemNaoLida(emailUsuario);
    }

    public void deletarUsuarioPorEmail(String email){
        if(pesquisarUsuarioExiste(email)){
            this.usuarioRepository.deleteById(email);
        }
    }

    //acessando funcionalidades do PostService
    public List<Post> pesquisaTodosPostPorEmailAutor(String emailAutor){
        return this.postService.pesquisaTodosPostPor(emailAutor);
    }

}
