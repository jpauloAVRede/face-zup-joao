package br.com.zup.FaceZup_Joao.usuario.dtos;

import br.com.zup.FaceZup_Joao.mensagem.Mensagem;
import org.springframework.context.annotation.Bean;

import java.util.List;
import java.util.stream.Collectors;

public class UsuarioMensagemIdsNaoLidaDTO {
    private int id;

    public UsuarioMensagemIdsNaoLidaDTO() {
    }

    public UsuarioMensagemIdsNaoLidaDTO(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static List<UsuarioMensagemIdsNaoLidaDTO> convertLista(List<Mensagem> listMensagem){
        return listMensagem.stream().map(mensagem -> new UsuarioMensagemIdsNaoLidaDTO(mensagem.getId())).collect(Collectors.toList());
    }

}
