package br.com.zup.FaceZup_Joao.usuario;

import br.com.zup.FaceZup_Joao.mensagem.Mensagem;
import br.com.zup.FaceZup_Joao.post.Post;
import br.com.zup.FaceZup_Joao.post.dtos.PostDTO;
import br.com.zup.FaceZup_Joao.usuario.dtos.CadastrarUsuarioDTO;
import br.com.zup.FaceZup_Joao.usuario.dtos.UsuarioMensagemIdsNaoLidaDTO;
import br.com.zup.FaceZup_Joao.usuario.dtos.UsuarioQtdMensagemAndIdNaoLidaDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
    private UsuarioService usuarioService;
    private ModelMapper modelMapper;

    @Autowired
    public UsuarioController(UsuarioService usuarioService, ModelMapper modelMapper) {
        this.usuarioService = usuarioService;
        this.modelMapper = modelMapper;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void cadastrarUsuario(@RequestBody @Valid CadastrarUsuarioDTO cadastrarUsuarioDTO){
        Usuario usuarioModel = modelMapper.map(cadastrarUsuarioDTO, Usuario.class);
        this.usuarioService.cadastrarUsuario(usuarioModel);
    }

    @GetMapping("/{emailUsuario}")
    public CadastrarUsuarioDTO pesquisaUsuarioPorEmail(@PathVariable String emailUsuario){
        Usuario usuarioModel = this.usuarioService.pesquisarUsuarioPorEmail(emailUsuario);
        return modelMapper.map(usuarioModel, CadastrarUsuarioDTO.class);
    }
/*
    @GetMapping("/usuario/perfil/{emailUsuario}")
    public List<UsuarioMensagemIdsNaoLidaDTO> pesquisaQtdMensagemNaoLida(@PathVariable String emailUsuario){
        List<Mensagem> lisIdMensagem = this.usuarioService.pesquisaListaIdMensagemNaoLida(emailUsuario);
        var qtdMensagemNaoLida = lisIdMensagem.size();
        return UsuarioMensagemIdsNaoLidaDTO.convertLista(lisIdMensagem);
    }

 */

    @GetMapping("/usuario/perfil/{emailUsuario}")
    public UsuarioQtdMensagemAndIdNaoLidaDTO pesquisaQtdMensagemNaoLida(@PathVariable String emailUsuario){
        List<Mensagem> lisIdMensagem = this.usuarioService.pesquisaListaIdMensagemNaoLida(emailUsuario);
        List<UsuarioMensagemIdsNaoLidaDTO> usuarioMensagemIdsNaoLidaDTO = UsuarioMensagemIdsNaoLidaDTO.convertLista(lisIdMensagem);

        UsuarioQtdMensagemAndIdNaoLidaDTO usuarioQtdMensagemAndIdNaoLidaDTO = new UsuarioQtdMensagemAndIdNaoLidaDTO(lisIdMensagem.size(), usuarioMensagemIdsNaoLidaDTO);
        return usuarioQtdMensagemAndIdNaoLidaDTO;
    }

    @DeleteMapping("{emailUsuario}")
    public void deletarUsuario(@PathVariable String emailUsuario){
        this.usuarioService.deletarUsuarioPorEmail(emailUsuario);
    }

    //Controller de Acesso dos Posts
    @GetMapping("/posts/{emailAutor}")
    public List<PostDTO> pesquisaTodosPostPorEmailAutor(@PathVariable String emailAutor){
        List<Post> postsModel = this.usuarioService.pesquisaTodosPostPorEmailAutor(emailAutor);
        return postsModel.stream()
                .map(post -> PostDTO.converterModelParaDTO(post))
                .collect(Collectors.toList());
    }

}
