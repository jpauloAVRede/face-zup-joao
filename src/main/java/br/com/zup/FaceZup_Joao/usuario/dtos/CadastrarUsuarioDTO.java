package br.com.zup.FaceZup_Joao.usuario.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CadastrarUsuarioDTO {
    @Email(message = "{validacao.email}")
    @NotBlank(message = "{validacao.email.vazio}")
    private String email;
    @Size(min = 2, max = 20, message = "{validacao.nome.sobrenome.cargo}")
    private String nome;
    @Size(min = 5, max = 20, message = "{validacao.nome.sobrenome.cargo}")
    private String sobrenome;
    @Size(min = 2, max = 30, message = "{validacao.nome.sobrenome.cargo}")
    private String cargo;

    public CadastrarUsuarioDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
}
