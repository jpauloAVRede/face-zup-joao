package br.com.zup.FaceZup_Joao.usuario;

import br.com.zup.FaceZup_Joao.comentario.Comentario;
import br.com.zup.FaceZup_Joao.mensagem.Mensagem;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.List;

@Entity
@Table(name = "usuarios")
public class Usuario {

    @Id
    @Column(unique = true, name = "email")
    @Email
    private String email;
    @Column(nullable = false)
    private String nome;
    @Column(nullable = false)
    private String sobrenome;
    @Column(nullable = false)
    private String cargo;
    @OneToMany(mappedBy = "usuario")
    private List<Mensagem> mensagems;
    @OneToMany(mappedBy = "usuarioAutor")
    private List<Comentario> comentarios;

    public Usuario() {
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public List<Mensagem> getMensagems() {
        return mensagems;
    }

    public void setMensagems(List<Mensagem> mensagems) {
        this.mensagems = mensagems;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
}
