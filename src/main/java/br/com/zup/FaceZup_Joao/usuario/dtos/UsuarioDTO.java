package br.com.zup.FaceZup_Joao.usuario.dtos;

import br.com.zup.FaceZup_Joao.usuario.Usuario;

public class UsuarioDTO {

    private String email;
    private String nome;
    private String sobrenome;

    public UsuarioDTO() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public static UsuarioDTO ConverterModelParaDTO(Usuario usuario){
        UsuarioDTO usuarioDTO = new UsuarioDTO();

        usuarioDTO.setEmail(usuario.getEmail());
        usuarioDTO.setNome(usuario.getNome());
        usuarioDTO.setSobrenome(usuario.getSobrenome());

        return usuarioDTO;
    }

}
