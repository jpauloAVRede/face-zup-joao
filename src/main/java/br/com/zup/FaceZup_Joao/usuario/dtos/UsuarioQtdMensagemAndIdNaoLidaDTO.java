package br.com.zup.FaceZup_Joao.usuario.dtos;

import java.util.List;

public class UsuarioQtdMensagemAndIdNaoLidaDTO {
    private int qtdMensagemNaoLida;
    private List<UsuarioMensagemIdsNaoLidaDTO> listIdsMensagemNaoLida;

    public UsuarioQtdMensagemAndIdNaoLidaDTO() {
    }

    public UsuarioQtdMensagemAndIdNaoLidaDTO(int qtdMensagemNaoLida, List<UsuarioMensagemIdsNaoLidaDTO> listIdsMensagemNaoLida) {
        this.qtdMensagemNaoLida = qtdMensagemNaoLida;
        this.listIdsMensagemNaoLida = listIdsMensagemNaoLida;
    }

    public int getQtdMensagemNaoLida() {
        return qtdMensagemNaoLida;
    }

    public void setQtdMensagemNaoLida(int qtdMensagemNaoLida) {
        this.qtdMensagemNaoLida = qtdMensagemNaoLida;
    }

    public List<UsuarioMensagemIdsNaoLidaDTO> getListIdsMensagemNaoLida() {
        return listIdsMensagemNaoLida;
    }

    public void setListIdsMensagemNaoLida(List<UsuarioMensagemIdsNaoLidaDTO> listIdsMensagemNaoLida) {
        this.listIdsMensagemNaoLida = listIdsMensagemNaoLida;
    }
}
