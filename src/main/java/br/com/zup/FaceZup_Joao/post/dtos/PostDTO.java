package br.com.zup.FaceZup_Joao.post.dtos;

import br.com.zup.FaceZup_Joao.comentario.dtos.ComentarioDTO;
import br.com.zup.FaceZup_Joao.post.Post;
import br.com.zup.FaceZup_Joao.usuario.dtos.UsuarioDTO;

import java.util.List;
import java.util.stream.Collectors;

public class PostDTO {
    private int id;
    private String texto;
    private UsuarioDTO usuarioAutorDTO;
    private List<ComentarioDTO> comentariosDTO;

    public PostDTO() {
    }

    public UsuarioDTO getUsuarioAutorDTO() {
        return usuarioAutorDTO;
    }

    public void setUsuarioAutorDTO(UsuarioDTO usuarioAutorDTO) {
        this.usuarioAutorDTO = usuarioAutorDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public List<ComentarioDTO> getComentariosDTO() {
        return comentariosDTO;
    }

    public void setComentariosDTO(List<ComentarioDTO> comentariosDTO) {
        this.comentariosDTO = comentariosDTO;
    }

    public static PostDTO converterModelParaDTO(Post post){
        PostDTO postDTO = new PostDTO();
        UsuarioDTO usuarioDTO = UsuarioDTO.ConverterModelParaDTO(post.getUsuarioAutor());
        List<ComentarioDTO> comentarioDTOS = post.getComentarios().stream()
                                                .map(comentario -> ComentarioDTO.converterModelParaDTO(comentario))
                                                .collect(Collectors.toList());

        postDTO.setId(post.getId());
        postDTO.setTexto(post.getTexto());
        postDTO.setUsuarioAutorDTO(usuarioDTO);
        postDTO.setComentariosDTO(comentarioDTOS);

        return postDTO;
    }

}
