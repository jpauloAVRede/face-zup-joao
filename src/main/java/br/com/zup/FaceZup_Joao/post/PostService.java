package br.com.zup.FaceZup_Joao.post;

import br.com.zup.FaceZup_Joao.usuario.Usuario;
import br.com.zup.FaceZup_Joao.usuario.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UsuarioService usuarioService;

    public void salvarPost(String textoPost, String emailUsuarioAutor){
        Post postModel = new Post();
        Usuario usuarioAutor = this.usuarioService.pesquisarUsuarioPorEmail(emailUsuarioAutor);

        postModel.setTexto(textoPost);
        postModel.setUsuarioAutor(usuarioAutor);

        this.postRepository.save(postModel);
    }

    public List<Post> pesquisaTodosPostPor(String emailAutor){
        return this.postRepository.findAllByUsuarioAutorEmail(emailAutor);
    }

    public List<Post> pesquisaTodosPostPor(){
        return (List<Post>) this.postRepository.findAll();
    }

    public Post pesquisaPostPorId(int idPost){
        return this.postRepository.findById(idPost).get();
    }

}
