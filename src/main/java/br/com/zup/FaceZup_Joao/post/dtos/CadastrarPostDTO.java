package br.com.zup.FaceZup_Joao.post.dtos;

import br.com.zup.FaceZup_Joao.usuario.Usuario;

import javax.validation.Valid;
import javax.validation.constraints.Size;

public class CadastrarPostDTO {
    @Size(min = 3, max = 100, message = "{validacao.mensagem}")
    private String texto;
    @Valid
    private String emailUsuarioAutor;

    public CadastrarPostDTO() {
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getEmailUsuarioAutor() {
        return emailUsuarioAutor;
    }

    public void setEmailUsuarioAutor(String emailUsuarioAutor) {
        this.emailUsuarioAutor = emailUsuarioAutor;
    }
}
