package br.com.zup.FaceZup_Joao.post;

import br.com.zup.FaceZup_Joao.comentario.Comentario;
import br.com.zup.FaceZup_Joao.usuario.Usuario;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String texto;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Usuario usuarioAutor;
    @OneToMany(mappedBy = "post")
    private List<Comentario> comentarios;

    public Post() {
    }

    public List<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Usuario getUsuarioAutor() {
        return usuarioAutor;
    }

    public void setUsuarioAutor(Usuario usuarioAutor) {
        this.usuarioAutor = usuarioAutor;
    }

}
