package br.com.zup.FaceZup_Joao.post;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostRepository extends CrudRepository<Post, Integer> {

    List<Post> findAllByUsuarioAutorEmail(String emailAutor);

}
