package br.com.zup.FaceZup_Joao.post;

import br.com.zup.FaceZup_Joao.post.dtos.CadastrarPostDTO;
import br.com.zup.FaceZup_Joao.post.dtos.PostDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/posts")
public class PostController {
    @Autowired
    private PostService postService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void salvarPost(@RequestBody @Valid CadastrarPostDTO cadastrarPostDTO){
        this.postService.salvarPost(cadastrarPostDTO.getTexto(), cadastrarPostDTO.getEmailUsuarioAutor());
    }

    @GetMapping("/{idPost}")
    public PostDTO pesquisarPostPorId(@PathVariable int idPost){
        Post postModel = this.postService.pesquisaPostPorId(idPost);
        return PostDTO.converterModelParaDTO(postModel);
    }

    @GetMapping
    public List<PostDTO> pesquisaTodosPosts(){
        List<Post> postsModel = this.postService.pesquisaTodosPostPor();
        List<PostDTO> postDTOS = postsModel.stream()
                                        .map(post -> PostDTO.converterModelParaDTO(post))
                                        .collect(Collectors.toList());
        return postDTOS;
    }

}
