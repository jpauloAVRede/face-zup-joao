package br.com.zup.FaceZup_Joao.comentario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ComentarioService {
    @Autowired
    private ComentarioRepository comentarioRepository;

    public void salvarComentario(Comentario comentarioModel){
        this.comentarioRepository.save(comentarioModel);
    }

}
