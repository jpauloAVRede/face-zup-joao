package br.com.zup.FaceZup_Joao.comentario;

import br.com.zup.FaceZup_Joao.post.Post;
import br.com.zup.FaceZup_Joao.usuario.Usuario;

import javax.persistence.*;

@Entity
@Table(name = "comentarios")
public class Comentario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String texto;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Usuario usuarioAutor;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Post post;

    public Comentario() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Usuario getUsuarioAutor() {
        return usuarioAutor;
    }

    public void setUsuarioAutor(Usuario usuarioAutor) {
        this.usuarioAutor = usuarioAutor;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

}
