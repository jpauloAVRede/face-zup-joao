package br.com.zup.FaceZup_Joao.comentario.dtos;

import br.com.zup.FaceZup_Joao.comentario.Comentario;
import br.com.zup.FaceZup_Joao.usuario.dtos.UsuarioDTO;

public class ComentarioDTO {

    private int id;
    private String texto;
    private UsuarioDTO usuarioAutorDTO;

    public ComentarioDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public UsuarioDTO getUsuarioAutorDTO() {
        return usuarioAutorDTO;
    }

    public void setUsuarioAutorDTO(UsuarioDTO usuarioAutorDTO) {
        this.usuarioAutorDTO = usuarioAutorDTO;
    }

    public static ComentarioDTO converterModelParaDTO(Comentario comentario){
        ComentarioDTO comentarioDTO = new ComentarioDTO();
        UsuarioDTO usuarioDTO = UsuarioDTO.ConverterModelParaDTO(comentario.getUsuarioAutor());

        comentarioDTO.setId(comentario.getId());
        comentarioDTO.setTexto(comentario.getTexto());
        comentarioDTO.setUsuarioAutorDTO(usuarioDTO);

        return comentarioDTO;
    }

}
