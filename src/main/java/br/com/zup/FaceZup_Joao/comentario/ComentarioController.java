package br.com.zup.FaceZup_Joao.comentario;

import br.com.zup.FaceZup_Joao.comentario.dtos.CadastrarComentarioDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comentarios")
public class ComentarioController {
    @Autowired
    private ComentarioService comentarioService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void salvarComentario(@RequestBody CadastrarComentarioDTO cadastrarComentarioDTO){
        Comentario comentarioModel = this.modelMapper.map(cadastrarComentarioDTO, Comentario.class);
        this.comentarioService.salvarComentario(comentarioModel);
    }


}
