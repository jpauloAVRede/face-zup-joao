package br.com.zup.FaceZup_Joao.comentario;

import org.springframework.data.repository.CrudRepository;

public interface ComentarioRepository extends CrudRepository<Comentario, Integer> {

}
