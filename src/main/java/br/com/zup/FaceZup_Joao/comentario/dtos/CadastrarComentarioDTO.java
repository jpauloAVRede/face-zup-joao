package br.com.zup.FaceZup_Joao.comentario.dtos;

import br.com.zup.FaceZup_Joao.post.Post;
import br.com.zup.FaceZup_Joao.usuario.Usuario;

import javax.validation.Valid;
import javax.validation.constraints.Size;

public class CadastrarComentarioDTO {
    @Size(min = 3, max = 100, message = "{validacao.mensagem}")
    private String texto;
    @Valid
    private Usuario usuarioAutorComentario;
    @Valid
    private Post post;

    public CadastrarComentarioDTO() {
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Usuario getUsuarioAutorComentario() {
        return usuarioAutorComentario;
    }

    public void setUsuarioAutorComentario(Usuario usuarioAutorComentario) {
        this.usuarioAutorComentario = usuarioAutorComentario;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

}
