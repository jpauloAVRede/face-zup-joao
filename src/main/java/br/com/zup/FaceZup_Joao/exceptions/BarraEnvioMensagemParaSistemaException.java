package br.com.zup.FaceZup_Joao.exceptions;

public class BarraEnvioMensagemParaSistemaException extends RuntimeException{

    public BarraEnvioMensagemParaSistemaException(String message) {
        super(message);
    }
}
