package br.com.zup.FaceZup_Joao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControleAdivisor {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro manipularExceptionsDeValidacao(MethodArgumentNotValidException exception){
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        List<Erro> erros = fieldErrors.stream().map(objeto -> new Erro(objeto.getDefaultMessage())).collect(Collectors.toList());

        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(UsuarioDuplicadoException.class)
    public MensagemDeErro manupularUsuarioDuplicadoException(UsuarioDuplicadoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));

        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(BarraEnvioMensagemParaSistemaException.class)
    public MensagemDeErro manipularUsuarioDuplicadoException(BarraEnvioMensagemParaSistemaException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(UsuarioNaoEncontradoException.class)
    public MensagemDeErro manipularUsuarioNaoEncontradoException(UsuarioNaoEncontradoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(MensagemNaoCadastradaException.class)
    public MensagemDeErro manipularMensagemNaoCadastradaException(MensagemNaoCadastradaException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }



}
