package br.com.zup.FaceZup_Joao.exceptions;

public class UsuarioNaoEncontradoException extends RuntimeException{

    public UsuarioNaoEncontradoException(String message) {
        super(message);
    }

}
