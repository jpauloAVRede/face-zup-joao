package br.com.zup.FaceZup_Joao.exceptions;

public class MensagemNaoCadastradaException extends RuntimeException{

    public MensagemNaoCadastradaException(String message) {
        super(message);
    }
}
