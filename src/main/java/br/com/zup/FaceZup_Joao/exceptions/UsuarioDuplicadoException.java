package br.com.zup.FaceZup_Joao.exceptions;

public class UsuarioDuplicadoException extends RuntimeException{

    public UsuarioDuplicadoException(String message) {
        super(message);
    }
}
