package br.com.zup.FaceZup_Joao.mensagem.dtos;

import java.time.LocalDateTime;

public class MensagemDTO {
    private int id;
    private String mensagem;
    private LocalDateTime dataEnvio;
    private boolean visualizado;

    public MensagemDTO() {
    }

    public LocalDateTime getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(LocalDateTime dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }
}
