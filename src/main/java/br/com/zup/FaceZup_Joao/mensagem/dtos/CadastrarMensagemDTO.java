package br.com.zup.FaceZup_Joao.mensagem.dtos;

import br.com.zup.FaceZup_Joao.mensagem.Mensagem;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CadastrarMensagemDTO {
    @Size(min = 2, max = 200, message = "{validacao.mensagem}")
    private String mensagem;
    @Email(message = "{validacao.email}")
    @NotBlank(message = "{validacao.email.vazio}")
    private String destinoMensagem;
    @Email(message = "{validacao.email}")
    @NotBlank(message = "{validacao.email.vazio}")
    private String origemMensagem;

    public CadastrarMensagemDTO() {
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getDestinoMensagem() {
        return destinoMensagem;
    }

    public void setDestinoMensagem(String destinoMensagem) {
        this.destinoMensagem = destinoMensagem;
    }

    public String getOrigemMensagem() {
        return origemMensagem;
    }

    public void setOrigemMensagem(String origemMensagem) {
        this.origemMensagem = origemMensagem;
    }

    public static Mensagem converterCadastrarMensagemDtoParaMensagemModel(CadastrarMensagemDTO cadastrarMensagemDTO){
        Mensagem mensagemModel = new Mensagem();
        mensagemModel.setMensagem(cadastrarMensagemDTO.getMensagem());

        return mensagemModel;
    }

}
