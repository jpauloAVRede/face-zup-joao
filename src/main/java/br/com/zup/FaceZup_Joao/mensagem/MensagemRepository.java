package br.com.zup.FaceZup_Joao.mensagem;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MensagemRepository extends CrudRepository<Mensagem, Integer> {

    Integer countAllByVisualizadoFalseAndDestinoUsuarioEmail(String emailUsuario);

    List<Mensagem> findAllByVisualizadoFalseAndDestinoUsuarioEmail(String emailUsuario);

}
