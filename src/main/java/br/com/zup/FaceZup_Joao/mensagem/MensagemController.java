package br.com.zup.FaceZup_Joao.mensagem;

import br.com.zup.FaceZup_Joao.mensagem.dtos.CadastrarMensagemDTO;
import br.com.zup.FaceZup_Joao.mensagem.dtos.MensagemDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/chat")
public class MensagemController {
    @Autowired
    private MensagemService mensagemService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public void cadastrarMensagem(@RequestBody @Valid CadastrarMensagemDTO cadastrarMensagemDTO){
        this.mensagemService.cadastrarMensagem(cadastrarMensagemDTO.getMensagem(), cadastrarMensagemDTO.getOrigemMensagem(), cadastrarMensagemDTO.getDestinoMensagem());
    }

    @GetMapping("/mensagem/{idMensagem}")
    public MensagemDTO lerMensagemPorId(@PathVariable int idMensagem){
        Mensagem mensagem = this.mensagemService.lerMensagemPorId(idMensagem);
        return modelMapper.map(mensagem, MensagemDTO.class);
    }

}
