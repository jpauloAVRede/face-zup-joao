package br.com.zup.FaceZup_Joao.mensagem;

import br.com.zup.FaceZup_Joao.usuario.Usuario;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "mensagens")
public class Mensagem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String mensagem;
    @Column(nullable = false)
    private boolean visualizado;
    @Column(name = "dada_envio", nullable = false)
    private LocalDateTime dataEnvio;
    @Column(name = "data_leitura")
    private LocalDateTime dataLeitura;

    @ManyToOne
    @JoinColumn(name = "origem", nullable = false)
    private Usuario usuario;

    @ManyToOne
    @JoinColumn(name = "destino", nullable = false)
    private Usuario destinoUsuario;

    public Mensagem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public boolean isVisualizado() {
        return visualizado;
    }

    public void setVisualizado(boolean visualizado) {
        this.visualizado = visualizado;
    }

    public LocalDateTime getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(LocalDateTime dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public LocalDateTime getDataLeitura() {
        return dataLeitura;
    }

    public void setDataLeitura(LocalDateTime dataLeitura) {
        this.dataLeitura = dataLeitura;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getDestinoUsuario() {
        return destinoUsuario;
    }

    public void setDestinoUsuario(Usuario destinoUsuario) {
        this.destinoUsuario = destinoUsuario;
    }
}
