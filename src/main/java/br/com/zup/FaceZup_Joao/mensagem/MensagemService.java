package br.com.zup.FaceZup_Joao.mensagem;

import br.com.zup.FaceZup_Joao.exceptions.BarraEnvioMensagemParaSistemaException;
import br.com.zup.FaceZup_Joao.exceptions.MensagemNaoCadastradaException;
import br.com.zup.FaceZup_Joao.usuario.Usuario;
import br.com.zup.FaceZup_Joao.usuario.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class MensagemService {
    @Autowired
    private MensagemRepository mensagemRepository;
    @Autowired
    private UsuarioService usuarioService;

    public Mensagem cadastrarMensagem(String mensagemEnvidada, String origemMensagem, String destinoMensagem){
        bloqueiaEnvioMensagemParaDestinoSistema(destinoMensagem);

        Usuario usuarioOrigem = this.usuarioService.pesquisarUsuarioPorEmail(origemMensagem);
        Usuario usuarioDestino = this.usuarioService.pesquisarUsuarioPorEmail(destinoMensagem);

        Mensagem mensagem = new Mensagem();

        mensagem.setMensagem(mensagemEnvidada);
        mensagem.setUsuario(usuarioOrigem);
        mensagem.setDestinoUsuario(usuarioDestino);
        mensagem.setDataEnvio(LocalDateTime.now());

        return this.mensagemRepository.save(mensagem);
    }

    private void bloqueiaEnvioMensagemParaDestinoSistema(String destinoMensagem){
        if (destinoMensagem.equals("sistema@email.com")){
            throw new BarraEnvioMensagemParaSistemaException("Não é possível enviar mensagem para o sistema");
        }
    }
/*
    public int pesquisaQtdMensagemNaoLida(String emailUsuario){
        this.usuarioService.pesquisarUsuarioExiste(emailUsuario);
        return this.mensagemRepository.countAllByVisualizadoFalseAndDestinoUsuarioEmail(emailUsuario);
    }

 */

    public List<Mensagem> pesquisaListaDeIdMensagemNaoLida(String emailUsuario){
        return this.mensagemRepository.findAllByVisualizadoFalseAndDestinoUsuarioEmail(emailUsuario);
    }

    public void verificaSeExisteMensagemPorId(int idMensagem){
        if (!this.mensagemRepository.existsById(idMensagem)){
            throw new MensagemNaoCadastradaException("Mensagem não cadastrada!!!");
        }
    }

    public Mensagem lerMensagemPorId(int idMensagem){
        verificaSeExisteMensagemPorId(idMensagem);

        Mensagem lerMensagem = this.mensagemRepository.findById(idMensagem).get();

        if (!lerMensagem.isVisualizado()){
            lerMensagem.setDataLeitura(LocalDateTime.now());
            lerMensagem.setVisualizado(true);
            this.mensagemRepository.save(lerMensagem);

            emailAutoSistemaEmailLido(lerMensagem.getDestinoUsuario().getNome(),
                    lerMensagem.getDestinoUsuario().getEmail(), lerMensagem.getUsuario().getEmail());

            return lerMensagem;
        }

        return lerMensagem;

    }

    public void emailAutoSistemaEmailLido(String nomeDetino, String emailDestino, String emailOrigem){
        if (!emailOrigem.equals("sistema@email.com")){
            String mensagemSistema = "O " +nomeDetino+ " leu sua mensagem. Talvez ele igone ou não";
            cadastrarMensagem(mensagemSistema, "sistema@email.com", emailDestino);
        }
    }

}
