package br.com.zup.FaceZup_Joao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaceZupJoaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FaceZupJoaoApplication.class, args);
	}

}
